import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniHeartComponent } from './mini-heart.component';

describe('MiniHeartComponent', () => {
  let component: MiniHeartComponent;
  let fixture: ComponentFixture<MiniHeartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MiniHeartComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MiniHeartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
