import { Component, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {

  @ViewChild('envelope') envelope!: ElementRef;
  @ViewChild('card') card!: ElementRef;
  @ViewChild('heart') heart!: ElementRef;
  @ViewChild('text') text!: ElementRef;
  @ViewChild('front') front!: ElementRef;

  constructor(private renderer: Renderer2) { }
  onValentinesDayClick(): void {
    this.startEnvelopeAnimation();
  }

  private startEnvelopeAnimation(): void {
    this.renderer.setStyle(this.envelope.nativeElement, 'animation', 'fall 3s linear 1');
    this.renderer.setStyle(this.envelope.nativeElement, 'webkitAnimation', 'fall 3s linear 1');

    setTimeout(() => {
      this.renderer.setStyle(this.envelope.nativeElement, 'display', 'none');
      this.hideElements();
      this.showCardWithAnimation();
    }, 200); // Coincide con la duración de fadeOut de jQuery
  }

  private hideElements(): void {
    this.renderer.setStyle(this.heart.nativeElement, 'display', 'none');
    this.renderer.setStyle(this.text.nativeElement, 'display', 'none');
    this.renderer.setStyle(this.front.nativeElement, 'display', 'none');
  }

  private showCardWithAnimation(): void {
    this.renderer.setStyle(this.card.nativeElement, 'visibility', 'visible');
    this.renderer.setStyle(this.card.nativeElement, 'opacity', '0');
    this.renderer.setStyle(this.card.nativeElement, 'transform', 'scale(0.1)');

    const start = performance.now();

    const animate = (time: number) => {
      let progress = (time - start) / 1000; // Convertimos el tiempo a segundos
      let opacity = Math.min(progress, 1);
      let scale = 1 + Math.sin(opacity * Math.PI) * 0.1;
      this.renderer.setStyle(this.card.nativeElement, 'opacity', opacity.toString());
      this.renderer.setStyle(this.card.nativeElement, 'transform', `scale(${scale})`);

      if (progress < 1) {
        requestAnimationFrame(animate);
      }
    };

    requestAnimationFrame(animate);
  }
}
